# README #

## Introduction

Examples for the Software Engineering Induciton (2019), linux and windows versions.  **Note** stick with the windows version unless you've got linux installed at home and have a really good reason to try it. I've not written a MacOS version, I don't have access to a Mac.  

## HelloWorld-Example  

An almost traditional example, this program outputs "Hello World" to the console.  The original book on the C programming language, by the languages authors used this as its first example.  Many books and tutorials on C and other languages have nodded to this same example.  

## OneNote-Example  

Plays one note. 

## Tune-Example 

Plays a tune (hopefully recognisable). 

