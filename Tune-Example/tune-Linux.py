# To run this on Windows though Idle (Run > Run Moduel)
# From the python interactive shell "whatever("tune-Windows.py")

#Linux version

import os
import time.

# Note: This reqiures beep is installed 
# e.g. (ubuntu)> apt-get install beep
# or   (fedora)> dnf install beep

# Use a command line call to beep
# takes two arguments 
# frequency -f 1500 (mhz)
# duration  -l 1000 (ms)

os.system("beep -f 261 -l 500") 
os.system("beep -f 392 -l 500")
os.system("beep -f 392 -l 500")
os.system("beep -f 440 -l 500")
os.system("beep -f 440 -l 500")
os.system("beep -f 392 -l 500")

# sleep takes a single argument
# time in seconds
time.sleep(0.5) # Silence of 0.5sec.

os.system("beep -f 394 -l 500")
os.system("beep -f 394 -l 500")
os.system("beep -f 324 -l 500")
os.system("beep -f 324 -l 500")
os.system("beep -f 239 -l 500")
os.system("beep -f 239 -l 500")
os.system("beep -f 261 -l 500")

