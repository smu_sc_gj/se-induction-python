# To run this on Windows though Idle (Run > Run Moduel)
# From the python interactive shell "whatever("oneNote-Windows.py")

import os

# Note: This reqiures beep is installed 
# e.g. (ubuntu)> apt-get install beep
# or   (fedora)> dnf install beep

# Use a command line call to beep
# takes two arguments 
# frequency -f 1500 (mhz)
# duration  -l 1000 (ms)

os.system("beep -f 1500 -l 100")

